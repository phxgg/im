# @universis/im

Universis api server instant messaging plugin

## Installation

    npm i @universis/im

## Configure

Register `@universis/im#MessagingService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/dining#MessagingService"
        }
    ]

Add `@universis/im#DiningSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/im#MessagingSchemaLoader"
                    }
                ]
            }
        }
    }
