module.exports = {
    sourceMaps: 'both',
    retainLines: true,
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    node: '14'
                }
            }
        ]
    ],
    plugins: [
        [
            "@babel/plugin-proposal-decorators",
            {
                "legacy": true
            }
        ],
        [
            "@babel/plugin-proposal-class-properties"
        ]
    ]
}
