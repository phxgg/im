/**
 * @param {DataEventArgs} event 
 * @param {Function} callback 
 */
export function beforeExecute(event, callback) {
  if (event && event.emitter && event.emitter.query && event.emitter.query.$expand) {
    /**
     * @type {Array<*>}
     */
    const expand = event.emitter.query.$expand;
    if (Array.isArray(expand)) {
      let item = expand.find((item) => {
        return item.$entity &&
          Object.prototype.hasOwnProperty.call(item.$entity, 'InstantMessageAccount');
      });

      if (item) {
        // create a new empty data object
        const testObject = event.model.convert({});
        const testProperty = testObject.property('imAccount');
        if (testProperty == null) {
          // do nothing and return
          return callback();
        }
        // try to upgrade and continue
        // (this operation is important on first use)
        return testProperty.migrate((err) => {
          if (err) {
            return callback(err);
          }
          if (item && item.$entity.$join == null) {
            // set left join to ensure zero or one multiplicity
            item.$entity.$join = 'left';
            item = expand.find((item) => {
              return item.$entity &&
                Object.prototype.hasOwnProperty.call(item.$entity, 'InstantMessageAccountData');
            });
            if (item && item.$entity.$join == null) {
              item.$entity.$join = 'left';
            }
          }
          return callback();
        });
      }
    }
  }
  return callback();
}
