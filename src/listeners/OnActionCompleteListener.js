import { DataError } from '@themost/common';
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
  //
}

async function afterSaveAsync(event) {
  // get context
  // eslint-disable-next-line no-unused-vars
  const context = event.model.context;
  // get current status
  const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
  // get previous status
  let previousStatus = 'UnknownActionStatus';
  if (event.previous) {
    previousStatus = event.previous.actionStatus.alternateName;
  } else {
    throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
  }
  if (actionStatus === 'CompletedActionStatus' && previousStatus !== 'CompletedActionStatus') {
    // place your code here
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}