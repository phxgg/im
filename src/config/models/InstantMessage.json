{
  "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
  "@id": "https://universis.io/schemas/InstantMessage",
  "name": "InstantMessage",
  "title": "InstantMessages",
  "hidden": true,
  "sealed": false,
  "abstract": false,
  "implements": "Thing",
  "version": "1.9.1",
  "fields": [
    {
      "name": "subject",
      "title": "Subject",
      "description": "The subject of this message.",
      "type": "Text"
    },
    {
      "name": "body",
      "title": "Body",
      "description": "The body of this message.",
      "type": "Text",
      "size": 8000
    },
    {
      "name": "sender",
      "title": "Sender",
      "description": "The email addresses of the author or authors of the message.",
      "type": "InstantMessageAccount",
      "calculation": "javascript:return this.model.context.model('User').where('name').equal(this.model.context.user.name).select('imAccount/id').value();",
      "nullable": false,
      "readonly": true,
      "editable": false
    },
    {
      "name": "from",
      "title": "From",
      "readonly": true,
      "insertable": false,
      "editable": false,
      "query": [
        {
          "$lookup": {
            "from": "InstantMessageAccounts",
            "localField": "sender",
            "foreignField": "id",
            "as": "sender"
          }
        },
        {
          "$project": {
            "from": "$sender.name"
          }
        }
    ],
    "privileges": [
      {
        "mask": 15,
        "type": "global"
      },
      {
        "mask": 15,
        "type": "global",
        "account": "Administrators"
      },
      {
        "@help": "allow read messages if user is member of a channel",
        "mask": 1,
        "type": "self",
        "filter": "recipient/members/account eq me()"
      },
      {
        "@help": "allow post messages if user is member of a channel",
        "mask": 2,
        "type": "self",
        "filter": "recipient/members/account eq me()"
      },
      {
        "@help": "allow update or remove messages sent by a user who is member of a channel",
        "mask": 12,
        "type": "self",
        "filter": "recipient/members/account eq me() and sender/account eq me()"
      }
    ],
    "constraints": [
    ],
    "seed": [
    ],
    "eventListeners": [
        {
            "type": "@themost/data/previous-state-listener"
        },
        {
          "type": "./listeners/OnSendingNewMessageListener"
        }
      ]
    },
    {
      "name": "cc",
      "title": "Cc",
      "description": "The email addresses of the secondary recipients of this message.",
      "type": "Text"
    },
    {
      "name": "bcc",
      "title": "Bcc",
      "description": "The email addresses of the hidden recipients of this message.",
      "type": "Text"
    },
    {
      "name": "dateReceived",
      "title": "Date Received",
      "description": "The date and time when the message was received.",
      "type": "DateTime"
    },
    {
      "name": "owner",
      "title": "Owner",
      "description": "The user who owns this message.",
      "type": "InstantMessageAccount",
      "nullable": false,
      "readonly": true,
      "editable": false,
      "calculation": "javascript:return this.model.context.model('User').where('name').equal(this.model.context.user.name).select('imAccount/id').value();"
    },
    {
      "name": "action",
      "title": "Action",
      "description": "The action associated with this message.",
      "type": "Action"
    },
    {
      "name": "identifier",
      "title": "Message Identifier",
      "description": "A string that represents a numeric or alphanumeric sequence which identifies uniquely the current item.",
      "nullable": false,
      "type": "Text",
      "size": 36,
      "value": "javascript:return this.newGuid();",
      "readonly": true
    },
    {
      "name": "about",
      "title": "About",
      "description": "About object reference",
      "type": "Text",
      "size": 255
    },
    {
      "name": "attachments",
      "type": "Attachment",
      "mapping": {
        "cascade": "delete",
        "associationType": "junction",
        "associationAdapter": "MessageAttachments"
      }
    },
    {
      "name": "recipient",
      "title": "Recipient",
      "type": "MessagingChannel",
      "nullable": false
    },
    {
      "name": "isReply",
      "title": "isReply",
      "description": "Determine if the instant message is a reply",
      "type": "Boolean",
      "nullable": true
    },
    {
      "name": "hasReply",
      "title": "hasReply",
      "description": "Determine if the instant message has at least one reply",
      "type": "Boolean",
      "nullable": true
    },
    {
      "name": "reactions",
      "type": "InstantMessageReaction",
      "nested": true,
      "expandable": true,
      "mapping": {
        "associationType": "association",
        "cascade": "delete",
        "parentModel": "InstantMessage",
        "parentField": "id",
        "childModel": "InstantMessageReaction",
        "childField": "instantMessage"
      }
    },
    {
      "name": "parentInstantMessage",
      "title": "Parent instant message id",
      "description": "Parent instant message id",
      "type": "Integer"
    }
  ],
  "privileges": [
    {
      "mask": 15,
      "type": "global"
    },
    {
      "mask": 15,
      "type": "global",
      "account": "Administrators"
    },
    {
      "@help": "allow read messages if user is member of a channel",
      "mask": 1,
      "type": "self",
      "filter": "recipient/members/account eq me()"
    },
    {
      "@help": "allow post messages if user is a channel maintainer",
      "mask": 2,
      "type": "self",
      "filter": "recipient/maintainer eq me()"
    },
    {
      "@help": "allow post messages if user is member of a channel and channel is not readonly",
      "mask": 2,
      "type": "self",
      "filter": "recipient/members/account eq me() and (recipient/readOnly eq false or recipient/readOnly eq null)"
    },
    {
      "@help": "allow update or remove messages sent by a user who is member of a channel",
      "mask": 12,
      "type": "self",
      "filter": "recipient/members/account eq me() and sender/account eq me()"
    }
  ],
  "constraints": [],
  "seed": [],
  "eventListeners": [
    {
      "type": "@themost/data/previous-state-listener"
    },
    {
      "type": "./listeners/OnSendingNewMessageListener"
    }
  ]
}